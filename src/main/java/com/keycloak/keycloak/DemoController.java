package com.keycloak.keycloak;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/demo")
public class DemoController {

    @GetMapping("/user")
    public String hello() {
        return "Hello from Spring boot & Keycloak";
    }

    @GetMapping()
    public String hello1() {
        return "Hello from Spring boot & Keycloak";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('admin')")
    public String hello2() {
        return "Hello from Spring boot & Keycloak - ADMIN";
    }
}